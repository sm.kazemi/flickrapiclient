package ir.smr.kazemi.flickrapiclient.data.model

import com.google.gson.annotations.SerializedName

/**
 * By the grace of Allah, Created by Sayed-MohammadReza Kazemi
 * on 12/18/20.
 */
data class ContactsDataModel(
    @SerializedName("contacts")
    val contacts: ContactsModel,

    @SerializedName("stat")
    val status: String
)