package ir.smr.kazemi.flickrapiclient.utility

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.view.View
import androidx.core.animation.addListener

/**
 * By the grace of Allah, Created by Sayed-MohammadReza Kazemi
 * on 12/9/20.
 */

class AnimUtils {

    companion object {
        val translationX = "translationX"

        fun animFlip(view: View, func: () -> Unit) {

            val oa1 = ObjectAnimator.ofFloat(view, "scaleX", 1f, 0f);
            val oa2 = ObjectAnimator.ofFloat(view, "scaleX", 0f, 1f);

            val anim = AnimatorSet().apply {
                play(oa1).before(oa2)
            }

            oa1.addListener(onEnd = {
                func()
            })

            anim.start()

        }
    }
}