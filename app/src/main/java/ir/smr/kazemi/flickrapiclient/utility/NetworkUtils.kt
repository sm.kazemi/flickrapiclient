package ir.smr.kazemi.flickrapiclient.utility

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build

/**
 * By the grace of Allah, Created by Sayed-MohammadReza Kazemi
 * on 12/11/20.
 */
class NetworkUtils {

    companion object {

        fun isConnected(context: Context): Boolean {

            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE)
                    as ConnectivityManager

            return if (Build.VERSION.SDK_INT < 29) {

                val ai = cm.activeNetworkInfo

                return ai != null && ai.isConnectedOrConnecting

            } else {

                var hasInternet = false

                cm.allNetworks.forEach {

                    val nc = cm.getNetworkCapabilities((it))

                    if (nc != null && nc.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)) {
                        hasInternet = true
                    }
                }

                hasInternet
            }
        }
    }


}