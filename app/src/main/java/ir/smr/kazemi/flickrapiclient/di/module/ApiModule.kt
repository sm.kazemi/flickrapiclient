package ir.smr.kazemi.flickrapiclient.di.module


import ApiService
import dagger.Module
import dagger.Provides
import ir.smr.kazemi.flickrapiclient.data.FlickrRepository
import ir.smr.kazemi.flickrapiclient.utility.BASEURL
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Singleton

/**
 * By the grace of Allah, Created by Sayed-MohammadReza Kazemi
 * on 10/10/20.
 */

@Module
class ApiModule {

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit {

        return Retrofit.Builder().apply {
            baseUrl(BASEURL)
            client(OkHttpClient())
            addConverterFactory(GsonConverterFactory.create())
        }.build()
    }

}