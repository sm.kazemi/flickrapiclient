package ir.smr.kazemi.flickrapiclient.data

import ApiService
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import ir.smr.kazemi.flickrapiclient.data.model.SearchPhotoModel
import ir.smr.kazemi.flickrapiclient.data.remote.ApiResult
import ir.smr.kazemi.flickrapiclient.extension.toLog
import retrofit2.Retrofit
import javax.inject.Inject

/**
 * By the grace of Allah, Created by Sayed-MohammadReza Kazemi
 * on 12/10/20.
 */
class FlickrRepository @Inject constructor(private val retrofit: Retrofit) {

    /**
     * Fetch all photos associated with the tag  by ApiService from the server
     *
     * @return synced data during the fetching by liveData
     * */
    fun searchWithTag(tag: String, page: Int): LiveData<ApiResult<SearchPhotoModel>> = liveData {

        val api = retrofit.create(
            ApiService::class.java
        )
        emit(ApiResult.loading<SearchPhotoModel>())

        try {

            val result = api.publicSearch(tag = tag, page = page)

            if (result.isSuccessful) {

                if (result.body() != null) {

                    emit(ApiResult.success(result.body()!!))

                } else {

                    emit(ApiResult.fail<SearchPhotoModel>())

                    "null".toLog()

                }

            } else {

                emit(ApiResult.fail<SearchPhotoModel>())
                "unSuccessful".toLog()
                result.message().toLog("mes")
                "${result.code()}".toLog()

            }

        } catch (e: Exception) {

            emit(ApiResult.fail<SearchPhotoModel>())
            e.message?.toLog("exception")

        }

    }
}