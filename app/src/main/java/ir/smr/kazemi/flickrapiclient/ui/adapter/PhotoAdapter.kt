package ir.smr.kazemi.flickrapiclient.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import ir.smr.kazemi.flickrapiclient.R
import ir.smr.kazemi.flickrapiclient.data.model.PhotoModel
import ir.smr.kazemi.flickrapiclient.extension.toLog
import ir.smr.kazemi.flickrapiclient.utility.AnimUtils
import kotlinx.android.synthetic.main.item_rec_main.view.*

class PhotoAdapter(
    private val photosList: ArrayList<PhotoModel>,
    private val nextPage: () -> Unit
) :
    RecyclerView.Adapter<PhotoAdapter.Vh>() {

    private var flipItemsPositionList = ArrayList<Int>()

    class Vh(val rec_item: View) : RecyclerView.ViewHolder(rec_item) {

        val imageView = rec_item.img

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Vh {


        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_rec_main, parent, false)

        return Vh(v)

    }

    override fun onBindViewHolder(holder: Vh, position: Int) {

        //init item
        holder.rec_item.apply {

            if (flipItemsPositionList.contains(position)) {

                if (holder.rec_item.linear_detail.visibility == View.GONE) {
                    holder.rec_item.linear_detail.visibility = View.VISIBLE

                } else {
                    holder.rec_item.linear_detail.visibility = View.GONE
                }

//                flipItemsPositionList.remove(position)

            }

        }


        val urlLink = generateImageUrl(position)


        // load image from url
        holder.imageView.load(urlLink) {
            crossfade(true)
            placeholder(R.drawable.ic_baseline_image_24)
            error(R.drawable.ic_baseline_broken_image_24)
        }

        // click listener for photos
        holder.rec_item.setOnClickListener {

            // flip and show details
            flipPhoto(it, holder, position)

        }


        // fetch new photos at the end of the current photos list
        if (position == (photosList.size - 1)) {

            nextPage()
        }

    }

    private fun flipPhoto(view: View, holder: Vh, position: Int) {

        // set title of photo
        holder.rec_item.linear_detail.txt_title_photo.text = photosList[position].title

        // animation
        AnimUtils.animFlip(view) {

            flipItemsPositionList.add(position)

            if (holder.rec_item.linear_detail.visibility == View.GONE) {
                holder.rec_item.linear_detail.visibility = View.VISIBLE

            } else {
                holder.rec_item.linear_detail.visibility = View.GONE
            }
        }
        "$position".toLog()

    }

    /**
     *  Each image url must be create from its fetched data
     *
     *  The url pattern of the images is as follows
     *  https://live.staticflickr.com/{server-id}/{id}_{secret}_{size-suffix}.jpg
     *
     *  The required variables are extracted from the image data
     *
     *  Read this page for more detail @{https://www.flickr.com/services/api/misc.urls.html}
     */
    private fun generateImageUrl(position: Int): String {

        val serverId = photosList[position].server
        val id = photosList[position].id
        val secret = photosList[position].secret

        return "https://live.staticflickr.com/$serverId/${id}_${secret}_w.jpg"

    }

    override fun getItemCount() = photosList.size

}
