package ir.smr.kazemi.flickrapiclient.di.component

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import ir.smr.kazemi.flickrapiclient.di.module.ApiModule
import ir.smr.kazemi.flickrapiclient.ui.MainActivity
import ir.smr.kazemi.flickrapiclient.ui.ViewModelMain
import javax.inject.Singleton

/**
 * By the grace of Allah, Created by Sayed-MohammadReza Kazemi
 */
@Singleton
@Component(modules = [ApiModule::class])
interface AppComponent {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Context): AppComponent
    }

    fun inject(mainActivity: MainActivity)
    fun inject(viewModelMain: ViewModelMain)

}