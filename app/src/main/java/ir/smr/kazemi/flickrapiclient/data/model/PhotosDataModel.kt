package ir.smr.kazemi.flickrapiclient.data.model

import com.google.gson.annotations.SerializedName

data class PhotosDataModel(

    @SerializedName("page")
    val page: Int,

    @SerializedName("pages")
    val pages: Int,

    @SerializedName("perpage")
    val perpage: String,

    @SerializedName("total")
    val total: String,

    @SerializedName("photo")
    val photoList: ArrayList<PhotoModel>
)
