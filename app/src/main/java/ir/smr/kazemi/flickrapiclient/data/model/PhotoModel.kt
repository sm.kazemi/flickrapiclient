package ir.smr.kazemi.flickrapiclient.data.model

import com.google.gson.annotations.SerializedName

data class PhotoModel(

    @SerializedName("id")
    val id: String,

    @SerializedName("owner")
    val owner: String,

    @SerializedName("secret")
    val secret: String,

    @SerializedName("server")
    val server: String,

    @SerializedName("title")
    val title: String,

    )