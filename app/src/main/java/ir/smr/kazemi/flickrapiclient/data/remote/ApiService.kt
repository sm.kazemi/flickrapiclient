import ir.smr.kazemi.flickrapiclient.data.model.SearchPhotoModel
import ir.smr.kazemi.flickrapiclient.utility.apiKey
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * By the grace of Allah, Created by Sayed-MohammadReza Kazemi
 * on 12/10/20.
 */

interface ApiService {

    @GET("rest/")
    suspend fun publicSearch(
        @Query("method") method: String = "flickr.photos.search",
        @Query("api_key") api_key: String = apiKey,
        @Query("tags") tag: String,
        @Query("format") format: String = "json",
        @Query("nojsoncallback") callback: String = "1",
        @Query("page") page: Int,
        @Query("per_page") per_page: Int = 10
    ): Response<SearchPhotoModel>

}