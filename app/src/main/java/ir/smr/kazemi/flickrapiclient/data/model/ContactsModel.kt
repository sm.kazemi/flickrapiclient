package ir.smr.kazemi.flickrapiclient.data.model

import com.google.gson.annotations.SerializedName

data class ContactsModel(
    @SerializedName("total")
    val total: Int
)
