package ir.smr.kazemi.flickrapiclient.data.remote

import ir.smr.kazemi.flickrapiclient.utility.ApiStatus


class ApiResult<out T>(val status: ApiStatus, val data: T?) {

    companion object {
        fun <T> success(data: T) = ApiResult<T>(ApiStatus.SUCCESS, data)
        fun <T> loading() = ApiResult<T>(ApiStatus.LOADING, null)
        fun <T> networkError() = ApiResult<T>(ApiStatus.NETWORK_ERROR, null)
        fun <T> fail(): ApiResult<T> = ApiResult<T>(ApiStatus.FAILED, null)
    }
}
