package ir.smr.kazemi.flickrapiclient.utility

/**
 * By the grace of Allah, Created by Sayed-MohammadReza Kazemi
 * on 12/9/20.
 */


val BASEURL = "https://www.flickr.com/services/"
val apiKey = "e92eec4d0d9b87920c91fa877ba46751"
val apiSecret = "069c89f3205740cf"

const val OK = "ok"
const val FAIL = "fail"

enum class ApiStatus {
    FAILED, NETWORK_ERROR, LOADING, SUCCESS
}