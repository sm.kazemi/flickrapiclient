package ir.smr.kazemi.flickrapiclient

import android.app.Application
import ir.smr.kazemi.flickrapiclient.di.component.AppComponent
import ir.smr.kazemi.flickrapiclient.di.component.DaggerAppComponent

/**
 * By the grace of Allah, Created by Sayed-MohammadReza Kazemi
 * on 9/27/20.
 */
class App : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.factory().create(applicationContext)
    }

}