package ir.smr.kazemi.flickrapiclient.ui

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.github.scribejava.apis.FlickrApi
import com.github.scribejava.core.builder.ServiceBuilder
import com.github.scribejava.core.model.OAuthRequest
import com.github.scribejava.core.model.Verb
import ir.smr.kazemi.flickrapiclient.App
import ir.smr.kazemi.flickrapiclient.R
import ir.smr.kazemi.flickrapiclient.data.model.PhotosDataModel
import ir.smr.kazemi.flickrapiclient.extension.longToast
import ir.smr.kazemi.flickrapiclient.extension.shortToast
import ir.smr.kazemi.flickrapiclient.extension.toLog
import ir.smr.kazemi.flickrapiclient.ui.ViewModelMain.s.code
import ir.smr.kazemi.flickrapiclient.ui.adapter.PhotoAdapter
import ir.smr.kazemi.flickrapiclient.utility.ApiStatus.*
import ir.smr.kazemi.flickrapiclient.utility.FAIL
import ir.smr.kazemi.flickrapiclient.utility.OK
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private lateinit var photoAdapter: PhotoAdapter
    private val viewModel: ViewModelMain by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        (application as App).appComponent.inject(viewModel)

        if (savedInstanceState == null) {
            initActivity()
        }


    }

    private fun initActivity() {

        viewModel.contacts.observe(this) {

            btn_auth.visibility = View.GONE

            if (it.status == OK) {
                txt_contact_number.text = it.contacts.total.toString()
            } else {

                longToast("get contact failed")

            }

        }


        // btn auth
        btn_auth.setOnClickListener {

            auth()
        }


        // recycler view
        initRecycler()

        fetchPhotos("Bike", 1)

    }

    private fun fetchPhotos(tag: String, page: Int) {

        // fetch photos
        viewModel.search(tag, page).observe(this) {

            when (it.status) {

                LOADING -> {
                    "loading".toLog()

                    // show progress bar
                    prg_main.visibility = View.VISIBLE
                }

                SUCCESS -> {

                    when (it.data?.status) {

                        OK -> {

                            updateUi(it.data.photosData)

                            shortToast("Successful operation")
                        }

                        FAIL -> {

                            // gone progress bar
                            prg_main.visibility = View.GONE

                            longToast(it.data.message)
                        }

                        else -> {
                            shortToast("Oh ...")
                        }
                    }

                }

                FAILED -> {
                    "f".toLog()

                    longToast("Failed, please try again")

                    // gone progress bar
                    prg_main.visibility = View.GONE
                }

                NETWORK_ERROR -> {

                    "n".toLog()

                    longToast("Failed, please check your internet connection")

                    // gone progress bar
                    prg_main.visibility = View.GONE
                }
            }

        }

    }

    // update recycler view with new photos
    private fun updateUi(photosData: PhotosDataModel) {

        // save last item position in recycler view
        val curSize = photoAdapter.itemCount

        // add new photos to recycler view list
        viewModel.updatePhotoData(photosData)

        // notify recycler view adapter for insert new photos
        photoAdapter.notifyItemRangeInserted(curSize, 10)

        // gone progress bar
        prg_main.visibility = View.GONE

    }

    private fun nextPage() {

        viewModel.page++

        if (viewModel.page < viewModel.pages)
            fetchPhotos("car", viewModel.page)

    }

    // initialize recycler view
    private fun initRecycler() {

        // add photos data to recyclerView Adapter
        photoAdapter = PhotoAdapter(viewModel.photosList) { nextPage() }

        rec_main.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(this@MainActivity, 3)
            adapter = photoAdapter
        }

        photoAdapter.notifyDataSetChanged()

    }

    /**
     * using scribejava library to implement flicker Auth
     *
     * 1- create auth url link
     * 2- open on browser
     * 3- get verifier code callback (@see onResume() )
     * 4- sign and request on private flicker api ( @see viewModel.getContact() )
     * 5- get result in json and convert to model ( @see viewModel.getContact() )
     *
     * */
    fun auth() {

        val apiKey = "e92eec4d0d9b87920c91fa877ba46751"
        val apiSecret = "069c89f3205740cf"

        ViewModelMain.service = ServiceBuilder(apiKey)
            .apiSecret(apiSecret)
            .callback("smr://callback")
            .build(FlickrApi.instance(FlickrApi.FlickrPerm.READ))

        GlobalScope.launch(Dispatchers.IO) {

            ViewModelMain.requestToken = ViewModelMain.service.requestToken

            val authorizationUrl =
                ViewModelMain.service.getAuthorizationUrl(ViewModelMain.requestToken)

            Log.d("TAG", "url $authorizationUrl")

            // open url
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(authorizationUrl)))

        }


    }


    override fun onResume() {
        super.onResume()

        val uri = intent?.data

        if (uri != null && uri.toString().startsWith("smr://callback")) {

            Log.d("TAG", "$uri")

            code = uri.getQueryParameter("oauth_verifier") ?: "0"

            viewModel.getContacts()

            Log.d("TAG", code)


        }
    }
}