package ir.smr.kazemi.flickrapiclient.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.github.scribejava.apis.FlickrApi
import com.github.scribejava.core.builder.ServiceBuilder
import com.github.scribejava.core.model.OAuth1RequestToken
import com.github.scribejava.core.model.OAuthRequest
import com.github.scribejava.core.model.Verb
import com.github.scribejava.core.oauth.OAuth10aService
import com.google.gson.Gson
import ir.smr.kazemi.flickrapiclient.data.FlickrRepository
import ir.smr.kazemi.flickrapiclient.data.model.ContactsDataModel
import ir.smr.kazemi.flickrapiclient.data.model.PhotoModel
import ir.smr.kazemi.flickrapiclient.data.model.PhotosDataModel
import ir.smr.kazemi.flickrapiclient.data.model.SearchPhotoModel
import ir.smr.kazemi.flickrapiclient.data.remote.ApiResult
import ir.smr.kazemi.flickrapiclient.utility.NetworkUtils
import ir.smr.kazemi.flickrapiclient.utility.apiKey
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * By the grace of Allah, Created by Sayed-MohammadReza Kazemi
 * on 12/18/20.
 */
class ViewModelMain : ViewModel() {

    companion object s {

        lateinit var code: String
        lateinit var requestToken: OAuth1RequestToken
        lateinit var service: OAuth10aService

    }


    var pages: Int = 0
    var page: Int = 1
    val photosList = ArrayList<PhotoModel>()

    val contacts = MutableLiveData<ContactsDataModel>()

    @Inject
    lateinit var repository: FlickrRepository

    @Inject
    lateinit var context: Context

    // get photos from webServices
    fun search(tag: String, page: Int): LiveData<ApiResult<SearchPhotoModel>> {

        // network check
        return if (NetworkUtils.isConnected(context)) {

            // call webservice
            repository.searchWithTag(tag, page)

        } else {

            MutableLiveData<ApiResult<SearchPhotoModel>>().apply {
                value = ApiResult.networkError()
            }

        }


    }

    // hold photos data
    fun updatePhotoData(photosData: PhotosDataModel) {

        pages = photosData.pages

        page = photosData.page

        photosList.addAll(photosData.photoList)

    }


    // sign and send request on flicker api to get contact list
    fun getContacts() {

        val PROTECTED_RESOURCE_URL = "https://api.flickr.com/services/rest"

        GlobalScope.launch {

            val accessToken = service.getAccessToken(requestToken, code)

            Log.d("TAG", "accessToken  ${accessToken.rawResponse}")
            Log.d("TAG", "token  ${accessToken.token}")

            val request = OAuthRequest(Verb.GET, PROTECTED_RESOURCE_URL)
            request.addQuerystringParameter("format", "json")
            request.addQuerystringParameter("nojsoncallback", "1")
            request.addQuerystringParameter("method", "flickr.contacts.getList")
            request.addQuerystringParameter("api_key", apiKey)
            service.signRequest(accessToken, request)

            try {

                var response = service.execute(request)

                Log.d("TAG", "body ${response.isSuccessful}")
                Log.d("TAG", "body ${response.body}")
                Log.d("TAG", "body ${response.message}")

                contacts.postValue(Gson().fromJson(response.body, ContactsDataModel::class.java))

            } catch (e: Exception) {

                Log.d("TAG", "${e.message}")

            }

        }


    }

}