package ir.smr.kazemi.flickrapiclient.data.model

import com.google.gson.annotations.SerializedName

/**
 * By the grace of Allah, Created by Sayed-MohammadReza Kazemi
 * on 12/10/20.
 */
data class SearchPhotoModel(

    @SerializedName("photos")
    val photosData: PhotosDataModel,

    @SerializedName("stat")
    val status: String,

    @SerializedName("message")
    val message: String

    )